package co.com.cfa.automation.challenge.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchProducts extends PageObject {

    public static final Target PRODUCTS = Target.the("Option for show all products").located(By.id("nav-menu-item-25690"));
    public static final Target PERSON = Target.the("Option for select or person type").located(By.id("nav-menu-item-25692"));

    public static final Target PRODUCTSOFPERSON = Target.the("Message for detail a products of people").located(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div/div/div/section[2]/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/section/div[2]/div/div[1]/div/div/div[1]/div/div/p"));
}
