package co.com.cfa.automation.challenge.questions;

import co.com.cfa.automation.challenge.userinterface.SearchProducts;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<Boolean> {

    private  String question;

    public Answer(String question) {
        this.question = question;
    }

    public static Answer toThe(String question) {
        return new Answer (question);

    }

    @Override
    public Boolean answeredBy (Actor actor){
        boolean result;
        String message = Text.of(SearchProducts.PRODUCTSOFPERSON).answeredBy(actor).toString();

        if (question.equals(message)){
            return true;
        }
        return false;
    }
}

