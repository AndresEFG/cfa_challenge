package co.com.cfa.automation.challenge.tasks;

import co.com.cfa.automation.challenge.userinterface.SearchProducts;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Search implements Task {

    public static Search the(){
        return Tasks.instrumented(Search.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(SearchProducts.PRODUCTS),
                Click.on(SearchProducts.PERSON)
                );

    }
}
