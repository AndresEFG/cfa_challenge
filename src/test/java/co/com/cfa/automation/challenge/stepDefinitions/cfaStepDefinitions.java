package co.com.cfa.automation.challenge.stepDefinitions;


import co.com.cfa.automation.challenge.questions.Answer;
import co.com.cfa.automation.challenge.tasks.OpenUp;

import co.com.cfa.automation.challenge.tasks.Search;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class cfaStepDefinitions {

@Before
public void setStage () {
    OnStage.setTheStage(new OnlineCast());
}


    @Given("a user to navigate in the cooperative web page")
    public void aUserToNavigateInTheCooperativeWebPage() {

        OnStage.theActorCalled("user").wasAbleTo(OpenUp.thePage());
    }


    @When("^The user click in the Products option$")
    public void theUserClickInTheProductsOption() throws Exception {

        OnStage.theActorInTheSpotlight().attemptsTo(Search.the());

    }

    @Then("^Appear all products of Cooperative$")
    public void appearAllProductsOfCooperative() throws Exception {

        String question = "Productos para ti";
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(question)));





    }
}
